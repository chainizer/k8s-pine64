#!/usr/bin/env bash

ACTION=$1
HOST_DIRECTORY=$2
ORIGINAL_IMAGE_PATH="$HOST_DIRECTORY/image.img"

function getAttachedLoops {
    losetup -j ${ORIGINAL_IMAGE_PATH} | sed -r "s/(\/dev\/loop[0-9]*):.*/\1/;"
}

function detachLoops {
    losetup -d $(getAttachedLoops | tr '\n' ' ')
}

function getImageOffsets {
    fdisk -l ${ORIGINAL_IMAGE_PATH} | grep -A2 -E "Device\s*Boot\s*Start\s*End" | tail -n +2 | sed -r "s/[^ ]* *([0-9]*).*$/\1/"
}

function getOffset {
    local index=$1
    getImageOffsets | head -n ${index} | tail -1
}

function getOffsetInByte {
    local index=$1
    local offset=$(getOffset ${index})
    echo $(($offset * 512))
}

function getLoop {
    local index=$1
    losetup -j ${ORIGINAL_IMAGE_PATH} | grep $(getOffsetInByte ${index}) | sed -r "s/(\/dev\/loop[0-9]*):.*/\1/;"
}

function mountImage {
    local index=$1
    local name=$2
    local type=$3
    local mnt=${HOST_DIRECTORY}/${name}

    mkdir -p ${mnt}

    losetup -f ${ORIGINAL_IMAGE_PATH} -o $(getOffsetInByte ${index})

    local loop=$(getLoop ${index})
    mount -t $3 ${loop} ${mnt}

    echo "mount $loop to $mnt"
}

function umountImage {
    local index=$1
    local name=$2
    local mnt=${HOST_DIRECTORY}/${name}
    umount ${mnt}
    echo "umount ${mnt}"
}

if [ "$ACTION" == "mount" ]; then
    detachLoops 2>/dev/null 1>&2
    mountImage 1 boot vfat
    mountImage 2 root auto
fi

if [ "$ACTION" == "umount" ]; then
    umountImage 1 boot
    umountImage 2 root
    detachLoops 2>/dev/null 1>&2
fi
