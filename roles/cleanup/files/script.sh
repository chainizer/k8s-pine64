#!/usr/bin/env bash

iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -F
iptables -t mangle -F
iptables -F
iptables -X

rm -rf /etc/cni
rm -rf /etc/kubernetes/
rm -rf /etc/docker/
rm -rf /var/log/pods/
rm -rf /var/log/containers/
rm -rf /var/lib/cni
rm -rf /run/flannel
rm -rf /root/.kube
rm -rf /home/pine64/.kube

ifconfig cni0 down
ifconfig flannel.1 down
ifconfig docker0 down

ip link delete cni0
ip link delete flannel.1
